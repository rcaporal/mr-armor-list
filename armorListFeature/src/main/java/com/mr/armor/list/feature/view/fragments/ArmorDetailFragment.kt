package com.mr.armor.list.feature.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mr.armor.list.feature.R
import com.mr.armor.list.feature.data.models.*
import com.mr.armor.list.feature.utils.fromJson
import com.mr.armor.list.feature.utils.load
import com.mr.armor.list.feature.utils.setupActionBar
import com.mr.armor.list.feature.utils.visible
import com.mr.armor.list.feature.view.menu_providers.BackButtonMenuProvider
import com.mr.armor.list.feature.view.renders.renderArmorCraftingMaterials
import com.mr.armor.list.feature.view.renders.renderArmorSkills
import com.mr.armor.list.feature.view.renders.renderArmorSlots
import com.mr.armor.list.feature.view.renders.returnArmorGender
import kotlinx.android.synthetic.main.fragment_armor_detail.*
import kotlinx.android.synthetic.main.layout_amor_details_error.*
import kotlinx.android.synthetic.main.layout_armor_crafting_container.*
import kotlinx.android.synthetic.main.layout_armor_details_slots.*
import kotlinx.android.synthetic.main.layout_armor_header.*
import kotlinx.android.synthetic.main.layout_armor_skills_container.*
import kotlinx.android.synthetic.main.layout_resistances.*
import org.koin.android.ext.android.inject

class ArmorDetailFragment : Fragment() {

	companion object {
		const val ARMOR_ARGUMENT = "ARMOR_ARGUMENT"
	}

	private val backButtonMenuProvider: BackButtonMenuProvider by inject()

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_armor_detail, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		setupActionBarForFragment()
		renderArmorDetails(getArmorFromArguments())
	}

	private fun getArmorFromArguments() =
		arguments?.getString(ARMOR_ARGUMENT).fromJson(Armor::class.java)

	private fun setupActionBarForFragment() {
		activity?.setupActionBar(
			title = R.string.armor_details_fragment_name,
			showBackButton = true
		)
		backButtonMenuProvider.addToActivityWithActionBar(
			activity,
			onBackPressed = {
				activity?.onBackPressedDispatcher?.onBackPressed()
			}
		)
	}

	private fun renderArmorDetails(armor: Armor?) {
		if (armor != null) {
			renderArmorImage(armor)

			renderArmorName(armor)

			renderArmorHeader(armor)

			renderArmorSlots(armor.slots)

			renderArmorResistances(armor.resistances)

			renderArmorSkills(armor.skills)

			renderArmorCrafting(armor.crafting)

			armorDetailsErrorLayout.visible(false)
		} else {
			armorDetailsErrorLayout.visible(true)
		}
	}

	private fun renderArmorImage(armor: Armor) {
		armorDetailImage.load(
			url = armor.assets?.imageMale ?: armor.assets?.imageFemale,
			onComplete = { success ->
				armorDetailImage.visible(success)
			}
		)
	}

	private fun renderArmorName(armor: Armor) {
		armorDetailsName.text = armor.name
		armorDetailsSet.text = armor.armorSet?.name
		armorDetailsGender.load(returnArmorGender(armor.attributes))
		armorDetailsSet.visible(!armor.armorSet?.name.isNullOrEmpty())
	}

	private fun renderArmorSlots(slots: List<ArmorSlot?>?) {
		if (slots?.isNotEmpty() == true) {
			armorDetailsSlots.renderArmorSlots(slots)
		} else {
			armorDetailsSlotsContainer?.visible(false)
		}
	}

	private fun renderArmorResistances(resistances: ArmorResistances?) {
		if (resistances != null) {
			armorDetailsResistancesFire.text = resistances.fire?.toString()
			armorDetailsResistancesFireContainer.visible(resistances.fire != null)

			armorDetailsResistancesDragon.text = resistances.dragon?.toString()
			armorDetailsResistancesDragonContainer.visible(resistances.dragon != null)

			armorDetailsResistancesWater.text = resistances.water?.toString()
			armorDetailsResistancesWaterContainer.visible(resistances.water != null)

			armorDetailsResistancesThunder.text = resistances.thunder?.toString()
			armorDetailsResistancesThunderContainer.visible(resistances.thunder != null)

			armorDetailsResistancesIce.text = resistances.ice?.toString()
			armorDetailsResistancesIceContainer.visible(resistances.ice != null)
		} else {
			armorDetailsResistancesContainer.visible(false)
		}
	}

	private fun renderArmorHeader(armor: Armor) {
		armorDetailsType.text = armor.type
		armorDetailsTypeContainer.visible(!armor.type.isNullOrEmpty())

		armorDetailsRank.text = armor.rank
		armorDetailsRankContainer.visible(!armor.rank.isNullOrEmpty())

		armorDetailsRarity.text = armor.rarity?.toString()
		armorDetailsRarityContainer.visible(armor.rarity != null)

		armorDetailsDefense.text = armor.defense?.base?.toString().plus("+")
		armorDetailsDefenseContainer.visible(armor.defense?.base != null)
	}

	private fun renderArmorSkills(skills: List<ArmorSkill?>?) {
		if (skills?.isNotEmpty() == true) {
			armorDetailsSkillsContainer.renderArmorSkills(skills)
		} else {
			armorDetailsSkillsContainer.visible(false)
		}
	}

	private fun renderArmorCrafting(crafting: ArmorCrafting?) {
		if (crafting?.materials?.isNotEmpty() == true) {
			armorDetailsCraftingContainer.renderArmorCraftingMaterials(crafting)
		} else {
			armorDetailsCraftingContainer.visible(false)
		}
	}

	override fun onDestroy() {
		backButtonMenuProvider.removeFromActivityWithActionBar(activity)
		super.onDestroy()
	}


}
