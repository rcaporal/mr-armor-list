package com.mr.armor.list.feature.data.models

data class ArmorSkillModifiers(
    val affinity: String?,
    val attack: String?,
    val damageDragon: String?,
    val damageFire: String?,
    val damageIce: String?,
    val damageThunder: String?,
    val damageWater: String?,
    val defense: String?,
    val health: String?,
    val resistDragon: String?,
    val resistFire: String?,
    val resistIce: String?,
    val resistThunder: String?,
    val resistWater: String?,
    val sharpnessBonus: String?
)
