package com.mr.armor.list.feature.data.remote_data_source

import com.mr.armor.list.feature.data.remote_data_source.RequestState.Error
import com.mr.armor.list.feature.data.remote_data_source.RequestState.Success
import kotlinx.coroutines.flow.flow

class ArmorListRemoteDataSourceImpl(private val armorListApiService: ArmorListApiService) :
	ArmorListRemoteDataSource {

	override fun getArmorList() = flow {
		try {
			emit(Success(armorListApiService.getArmorList()))
		}catch (e: Exception){
			emit(Error(e))
		}
	}
}


