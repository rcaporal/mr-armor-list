package com.mr.armor.list.feature.data.models

data class ArmorResistances(
    val dragon: Int?,
    val fire: Int?,
    val ice: Int?,
    val thunder: Int?,
    val water: Int?
)
