package com.mr.armor.list.feature.utils

import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.mr.armor.list.feature.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

fun View?.visible(visible: Boolean?) {
	this?.visibility = if (visible == true) View.VISIBLE else View.GONE
}

fun ImageView?.load(
	url: String?,
	showPlaceHolder: Boolean = true,
	onComplete: (success: Boolean) -> Unit = {}
) {
	if (!url.isNullOrEmpty()) {
		val callback = object : Callback {
			override fun onSuccess() {
				onComplete(true)
			}

			override fun onError(e: Exception?) {
				onComplete(false)
			}
		}

		Picasso.get().load(url).apply {
			if (showPlaceHolder) {
				placeholder(R.color.grey)
			}
		}.into(this, callback)

	} else {
		onComplete(false)
	}
}

fun ImageView?.load(@DrawableRes resource: Int?) {
	resource?.let {
		this?.setImageResource(resource)
	}
}
