package com.mr.armor.list.feature.view.adapters

import android.annotation.SuppressLint
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.utils.filterArmorsByQuery

class ArmorListAdapterFilter : Filter() {

	private var callback: AdapterSearchListener? = null
	private var armorList: MutableList<Armor> = mutableListOf()
	private var armorListFiltered: MutableList<Armor> = mutableListOf()
	private var adapter: Adapter<*>? = null

	fun setup(
		adapter: Adapter<*>,
		armorList: MutableList<Armor>,
		armorListFiltered: MutableList<Armor>
	) {
		this.armorList = armorList
		this.armorListFiltered = armorListFiltered
		this.adapter = adapter
	}

	fun setSearchCallback(searchListener: AdapterSearchListener?) {
		this.callback = searchListener
	}

	override fun performFiltering(constraint: CharSequence?): FilterResults {

		armorListFiltered.clear()
		armorListFiltered.addAll(armorList.filterArmorsByQuery(constraint))

		return FilterResults().apply { values = armorListFiltered }
	}

	@SuppressLint("NotifyDataSetChanged")
	override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
		if (armorListFiltered.isEmpty()) {
			callback?.whenNoItemWereFound()
		} else {
			callback?.whenItemsWereFound()
		}
		adapter?.notifyDataSetChanged()
	}
}
