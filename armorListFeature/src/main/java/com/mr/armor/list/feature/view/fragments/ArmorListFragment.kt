package com.mr.armor.list.feature.view.fragments

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.mr.armor.list.feature.R
import com.mr.armor.list.feature.data.remote_data_source.RequestState.*
import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.utils.networkErrorMessageResource
import com.mr.armor.list.feature.utils.setupActionBar
import com.mr.armor.list.feature.utils.toJson
import com.mr.armor.list.feature.utils.visible
import com.mr.armor.list.feature.view.adapters.AdapterSearchListener
import com.mr.armor.list.feature.view.adapters.ArmorCardClickListener
import com.mr.armor.list.feature.view.adapters.ArmorListAdapter
import com.mr.armor.list.feature.view.viewModels.ArmorListViewModel
import kotlinx.android.synthetic.main.fragment_armor_list.*
import kotlinx.android.synthetic.main.layout_loading.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ArmorListFragment : Fragment(), ArmorCardClickListener, AdapterSearchListener {

	companion object {
		const val TYPING_INTERVAL_MILLISECONDS: Long = 500
	}

	private val armorListViewModel: ArmorListViewModel by viewModel()
	private val armorListAdapter: ArmorListAdapter by inject()

	private var searchWithTypingIntervalJob: Job? = null

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View = inflater.inflate(R.layout.fragment_armor_list, container, false)

	override fun onResume() {
		super.onResume()
		setupActionBarForFragment()
	}

	private fun setupActionBarForFragment() {
		activity.setupActionBar(title = R.string.armor_list_fragment_name)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		setupSearchField()
		setupArmorListRecyclerView()

		fetchArmorList()
	}

	private fun fetchArmorList() {
		viewLifecycleOwner.lifecycleScope.launch {
			armorListViewModel.armorListResult
				.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
				.collect { result ->
					when (result) {
						is Loading -> {
							loadingLayout.visible(true)
						}
						is Error -> {
							context?.let {
								showErrorDialog(result.exception)
							}
						}
						is Success -> {
							loadingLayout.visible(false)
							armorListAdapter.updateArmorList(result.data)
						}
					}
				}
		}
	}

	private fun showErrorDialog(exception: Exception?) {
		context?.let {
			MaterialDialog(it).show {
				lifecycleOwner(viewLifecycleOwner)
				title(R.string.request_error_title)
				message(
					exception?.networkErrorMessageResource()
						?: R.string.request_error_message
				)
				positiveButton(R.string.retry_button) {
					armorListViewModel.tryAgain()
				}
				negativeButton(R.string.cancel_button) {
					this@ArmorListFragment.activity?.finish()
				}
				cancelable(false)
			}
		}
	}


	private fun setupSearchField() {

		armorSearchEditText.addTextChangedListener(
			afterTextChanged = { text: Editable? ->
				clearFilterButton.visible(!text.isNullOrEmpty())
				searchForArmorAttributes(text)
			}
		)

		clearFilterButton.setOnClickListener {
			armorSearchEditText.text.clear()
		}

		armorSearchEditText.setOnEditorActionListener { textView, actionId, _ ->
			if (actionId == IME_ACTION_DONE) {
				armorSearchEditText.clearFocus()
				searchForArmorAttributes(textView?.text)
			}
			return@setOnEditorActionListener false
		}
	}

	private fun searchForArmorAttributes(query: CharSequence?) {
		if (query != null) {
			armorListRecyclerView.stopScroll()
			searchWithTypingIntervalJob?.cancel()
			searchWithTypingIntervalJob = viewLifecycleOwner.lifecycleScope.launch {
				delay(TYPING_INTERVAL_MILLISECONDS)
				if (armorListRecyclerView.scrollState == SCROLL_STATE_IDLE) {
					armorListAdapter.filter.filter(query)
				}
			}
		}
	}

	private fun setupArmorListRecyclerView() {
		armorListAdapter.addSearchListener(this)
		armorListAdapter.addArmorClickListener(this)
		armorListRecyclerView.layoutManager = LinearLayoutManager(requireContext())
		armorListRecyclerView.adapter = armorListAdapter
	}

	override fun onArmorCardCLick(armor: Armor) {
		findNavController().navigate(
			R.id.action_armorListFragment_to_armorDetailFragment,
			bundleOf(ArmorDetailFragment.ARMOR_ARGUMENT to armor.toJson())
		)
	}

	override fun whenItemsWereFound() {
		armorListEmptyStateView.visible(false)
	}

	override fun whenNoItemWereFound() {
		armorListEmptyStateView.visible(true)
	}

}
