package com.mr.armor.list.feature.utils

import com.google.gson.Gson

fun Any?.toJson(): String {
	return try {
		Gson().toJson(this)
	} catch (e: Exception) {
		""
	}
}

fun <T> String?.fromJson(objectClass: Class<T>): T? {
	return try {
		Gson().fromJson(this, objectClass)
	} catch (e: Exception) {
		null
	}
}
