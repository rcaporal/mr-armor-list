package com.mr.armor.list.feature.utils

import androidx.annotation.StringRes
import com.google.gson.JsonSyntaxException
import com.mr.armor.list.feature.R
import retrofit2.HttpException
import java.io.IOException

@StringRes
fun Exception.networkErrorMessageResource(): Int {
	return when(this) {
		is IOException -> R.string.request_no_internet_error_message
		is JsonSyntaxException, is HttpException -> R.string.request_serve_side_error_message
		else -> R.string.request_error_message
	}
}
