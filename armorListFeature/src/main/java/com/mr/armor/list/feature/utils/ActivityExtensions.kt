package com.mr.armor.list.feature.utils

import android.app.Activity
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity

fun Activity?.setupActionBar(@StringRes title: Int, showBackButton: Boolean = false) {
	val toolbar = (this as? AppCompatActivity)?.supportActionBar
	toolbar?.title = this?.resources?.getString(title)
	toolbar?.setDisplayHomeAsUpEnabled(showBackButton)
	toolbar?.setDisplayShowHomeEnabled(showBackButton)
}
