package com.mr.armor.list.feature.data.models

data class ArmorSkill(
	val description: String?,
	val id: Int?,
	val level: Int?,
	val modifiers: ArmorSkillModifiers?,
	val skill: Int?,
	val skillName: String?
)
