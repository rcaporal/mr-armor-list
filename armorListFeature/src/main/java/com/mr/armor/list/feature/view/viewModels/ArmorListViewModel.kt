package com.mr.armor.list.feature.view.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.data.remote_data_source.ArmorListRemoteDataSource
import com.mr.armor.list.feature.data.remote_data_source.RequestState
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch


class ArmorListViewModel(private val armorListRemoteDataSource: ArmorListRemoteDataSource) : ViewModel() {

	init {
		fetchArmorList()
	}

	private var armorListJob: Job? = null
	private val _result = MutableStateFlow<RequestState<List<Armor>>>(RequestState.Loading)

	val armorListResult: StateFlow<RequestState<List<Armor>>> = _result.asStateFlow()
		.stateIn(
			initialValue = RequestState.Loading,
			scope = viewModelScope,
			started = WhileSubscribed(5000)
		)

	private fun fetchArmorList() {
		armorListJob?.cancel()
		armorListJob = viewModelScope.launch {
			armorListRemoteDataSource.getArmorList().collect {
				_result.value = it
			}
		}
	}

	fun tryAgain() {
		fetchArmorList()
	}
}
