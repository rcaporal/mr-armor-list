package com.mr.armor.list.feature.di

import com.mr.armor.list.feature.R
import com.mr.armor.list.feature.data.remote_data_source.ArmorListApiService
import com.mr.armor.list.feature.data.remote_data_source.ArmorListRemoteDataSource
import com.mr.armor.list.feature.data.remote_data_source.ArmorListRemoteDataSourceImpl
import com.mr.armor.list.feature.view.adapters.ArmorListAdapter
import com.mr.armor.list.feature.view.adapters.ArmorListAdapterFilter
import com.mr.armor.list.feature.view.menu_providers.BackButtonMenuProvider
import com.mr.armor.list.feature.view.menu_providers.BackButtonMenuProviderImpl
import com.mr.armor.list.feature.view.viewModels.ArmorListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private val networkModule = module {
	factory {
		Retrofit.Builder()
			.baseUrl(androidContext().resources.getString(R.string.base_url))
			.addConverterFactory(GsonConverterFactory.create())
			.build()
			.create(ArmorListApiService::class.java)
	}
}

private val dataModule = module {
	single<ArmorListRemoteDataSource> {
		ArmorListRemoteDataSourceImpl(
			get()
		)
	}
}

private val viewModule = module {
	viewModel { ArmorListViewModel(get()) }
	factory { ArmorListAdapterFilter() }
	factory { ArmorListAdapter(get()) }
	single<BackButtonMenuProvider> { BackButtonMenuProviderImpl() }
}

val armorListModules = listOf(
	networkModule,
	dataModule,
	viewModule
)
