package com.mr.armor.list.feature.data.models

data class ArmorAssets(
    val imageFemale: String?,
    val imageMale: String?
)
