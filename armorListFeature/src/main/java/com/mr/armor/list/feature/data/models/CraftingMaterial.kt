package com.mr.armor.list.feature.data.models

data class CraftingMaterial(
	val item: CraftingMaterialItem?,
	val quantity: Int?
)
