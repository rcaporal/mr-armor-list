package com.mr.armor.list.feature.data.remote_data_source

sealed interface RequestState<out T> {
	data class Success<T>(val data: T) : RequestState<T>
	data class Error(val exception: Exception? = null) : RequestState<Nothing>
	object Loading : RequestState<Nothing>
}
