package com.mr.armor.list.feature.utils

import com.mr.armor.list.feature.data.models.Armor

fun List<Armor>.filterArmorsByQuery(query: CharSequence?): List<Armor> {

	val searchableQuery = query.toSearchableContent()
	val numberQuery: Int? = try {
		searchableQuery.toInt()
	} catch (e: Exception) {
		null
	}

	return if (searchableQuery.isEmpty()) {
		this
	} else if (numberQuery != null) {
		this.filter {
			(it.defense?.base != null && it.defense.base >= numberQuery)
		}
	} else {
		this.filter {
			(it.name.toSearchableContent().contains(searchableQuery)) or
					(it.type.toSearchableContent().contains(searchableQuery)) or
					(it.rank.toSearchableContent().contains(searchableQuery)) or
					(it.armorSet?.name.toSearchableContent().contains(searchableQuery))
		}
	}
}
