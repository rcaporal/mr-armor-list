package com.mr.armor.list.feature.view.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.mr.armor.list.feature.R
import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.utils.load
import com.mr.armor.list.feature.utils.visible
import com.mr.armor.list.feature.view.renders.renderArmorSlots
import com.mr.armor.list.feature.view.renders.returnArmorAssetByType
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_armor_card.*

class ArmorCardViewHolder(
	itemView: View,
	private val armorCardClickListener: ArmorCardClickListener?
) : ViewHolder(itemView), LayoutContainer {
	override val containerView: View = itemView

	private fun unBind() {
		armorCard.setOnClickListener(null)
		armorCardDecorationSlots.removeAllViews()
	}

	fun bindTo(armor: Armor) {
		unBind()

		armorCard.setOnClickListener {
			armorCardClickListener?.onArmorCardCLick(armor)
		}
		val name = armor.name
		val rank = armor.rank
		val baseDefense = armor.defense?.base
		val type = armor.type

		armorCardTitle.text = name
		armorCardTitle.visible(!name.isNullOrEmpty())

		armorCardRankText.text = rank
		armorCardRankText.visible(!rank.isNullOrEmpty())

		armorCardDefenseText.text = baseDefense.toString().plus("+")
		armorCardDefenseText.visible(baseDefense != null)

		armorCardTypeImage.load(returnArmorAssetByType(type))
		armorCardTypeImage.visible(!type.isNullOrEmpty())

		armorCardDecorationSlots?.renderArmorSlots(armor.slots)

		val assetImage = armor.assets?.imageMale ?: armor.assets?.imageFemale

		armorCardImage.load(assetImage, showPlaceHolder = true) { success ->
			if (!success) {
				armorCardImage.load(R.drawable.ic_image_error)
			}
		}
	}
}
