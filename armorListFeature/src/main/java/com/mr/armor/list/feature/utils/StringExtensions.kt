package com.mr.armor.list.feature.utils

fun String?.toSearchableContent(): String {
	return if (this.isNullOrEmpty()) "" else {
		this.lowercase().replace(" ", "")
	}
}

fun CharSequence?.toSearchableContent(): String {
	return if (this.isNullOrEmpty()) "" else {
		this.toString().toSearchableContent()
	}
}
