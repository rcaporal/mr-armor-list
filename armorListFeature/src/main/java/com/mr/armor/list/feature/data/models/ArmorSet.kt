package com.mr.armor.list.feature.data.models

data class ArmorSet(
    val bonus: Int? = null,
    val id: Int? = null,
    val name: String? = null,
    val pieces: MutableList<Int?>? = null,
    val rank: String? = null
)
