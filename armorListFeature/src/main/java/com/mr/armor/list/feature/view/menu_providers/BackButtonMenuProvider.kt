package com.mr.armor.list.feature.view.menu_providers

import androidx.activity.ComponentActivity
import androidx.core.view.MenuProvider

interface BackButtonMenuProvider : MenuProvider {
	fun addToActivityWithActionBar(activity: ComponentActivity?, onBackPressed: () -> Unit)
	fun removeFromActivityWithActionBar(activity: ComponentActivity?)
}
