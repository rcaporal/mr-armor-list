package com.mr.armor.list.feature.view.menu_providers

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.ComponentActivity

class BackButtonMenuProviderImpl : BackButtonMenuProvider {

	private var onBackPressed: () -> Unit = {}

	override fun addToActivityWithActionBar(activity: ComponentActivity?, onBackPressed: () -> Unit) {
		activity?.addMenuProvider(this)
		this.onBackPressed = onBackPressed
	}

	override fun removeFromActivityWithActionBar(activity: ComponentActivity?) {
		activity?.removeMenuProvider(this)
		this.onBackPressed = {}
	}

	override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {}

	override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
		return if (menuItem.itemId == android.R.id.home) {
			onBackPressed()
			true
		} else {
			false
		}
	}
}
