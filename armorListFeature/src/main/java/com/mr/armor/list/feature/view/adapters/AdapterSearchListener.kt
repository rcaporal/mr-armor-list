package com.mr.armor.list.feature.view.adapters

interface AdapterSearchListener {
	fun whenItemsWereFound()
	fun whenNoItemWereFound()
}
