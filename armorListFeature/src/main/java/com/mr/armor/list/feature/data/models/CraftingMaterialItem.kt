package com.mr.armor.list.feature.data.models

data class CraftingMaterialItem(
    val carryLimit: Int?,
    val description: String?,
    val id: Int?,
    val name: String?,
    val rarity: Int?,
    val value: Int?
)
