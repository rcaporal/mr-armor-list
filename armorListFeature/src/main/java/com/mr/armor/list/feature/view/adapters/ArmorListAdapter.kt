package com.mr.armor.list.feature.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.mr.armor.list.feature.R
import com.mr.armor.list.feature.data.models.Armor

class ArmorListAdapter(private val adapterFilter: ArmorListAdapterFilter) :
	Adapter<ArmorCardViewHolder>(), Filterable {

	private var armorList: MutableList<Armor> = mutableListOf()
	private var armorListFiltered: MutableList<Armor> = mutableListOf()
	private var armorCardClickListener: ArmorCardClickListener? = null

	init {
		adapterFilter.setup(this, armorList, armorListFiltered)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArmorCardViewHolder {
		val view =
			LayoutInflater.from(parent.context).inflate(R.layout.item_armor_card, parent, false)
		return ArmorCardViewHolder(view, armorCardClickListener)
	}

	override fun onBindViewHolder(holder: ArmorCardViewHolder, position: Int) {
		return holder.bindTo(armorListFiltered[position])
	}

	override fun getItemCount(): Int = armorListFiltered.size

	@SuppressLint("NotifyDataSetChanged")
	fun updateArmorList(armorList: List<Armor>) {
		if (this.armorList.isEmpty()) {
			this.armorListFiltered.clear()
			this.armorListFiltered.addAll(armorList)
			notifyDataSetChanged()
		}

		this.armorList.clear()
		this.armorList.addAll(armorList)
	}

	override fun getFilter() = this.adapterFilter

	fun addSearchListener(callback: AdapterSearchListener?) {
		adapterFilter.setSearchCallback(callback)
	}

	fun addArmorClickListener(callback: ArmorCardClickListener?) {
		this.armorCardClickListener = callback
	}

}
