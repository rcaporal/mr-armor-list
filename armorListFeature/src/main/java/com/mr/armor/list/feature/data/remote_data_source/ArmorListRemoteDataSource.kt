package com.mr.armor.list.feature.data.remote_data_source

import com.mr.armor.list.feature.data.models.Armor
import kotlinx.coroutines.flow.Flow

interface ArmorListRemoteDataSource {
	fun getArmorList(): Flow<RequestState<List<Armor>>>
}
