package com.mr.armor.list.feature.view.renders

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import com.google.android.flexbox.FlexboxLayout
import com.mr.armor.list.feature.R
import com.mr.armor.list.feature.data.models.ArmorAttributes
import com.mr.armor.list.feature.data.models.ArmorCrafting
import com.mr.armor.list.feature.data.models.ArmorSkill
import com.mr.armor.list.feature.data.models.ArmorSlot
import com.mr.armor.list.feature.utils.visible

fun FlexboxLayout?.renderArmorSlots(
	slots: List<ArmorSlot?>?
) {
	if (slots?.isNotEmpty() == true) {
		val inflater = LayoutInflater.from(this?.context)
		slots.forEach { decoration ->
			decoration?.rank?.let { rank ->
				val decorationView: View? =
					inflater.inflate(
						R.layout.item_decoration_slot,
						this,
						false
					)
				val rankText: TextView? =
					decorationView?.findViewById(R.id.armorCardDecorationSlotText)
				rankText?.text = rank.toString()
				this?.addView(decorationView)
			}
		}
	}
	this.visible(slots?.isNotEmpty() == true)
}

fun LinearLayout?.renderArmorSkills(
	skills: List<ArmorSkill?>?
) {
	if (skills?.isNotEmpty() == true) {
		val inflater = LayoutInflater.from(this?.context)
		skills.forEach { skill ->
			if (!skill?.skillName.isNullOrEmpty() && !skill?.description.isNullOrEmpty()) {
				val skillView: View? =
					inflater.inflate(
						R.layout.item_armor_skill,
						this,
						false
					)

				val skillName: TextView? =
					skillView?.findViewById(R.id.armorDetailsSkillName)

				val skillDescription: TextView? =
					skillView?.findViewById(R.id.armorDetailsSkillDescription)

				skillName?.text = skill?.skillName
				skillDescription?.text = skill?.description

				this?.addView(skillView)
			}
		}
	}
}

fun LinearLayout?.renderArmorCraftingMaterials(
	armorCrafting: ArmorCrafting?
) {
	if (armorCrafting?.materials?.isNotEmpty() == true) {
		val inflater = LayoutInflater.from(this?.context)
		armorCrafting.materials.forEach { craftingMaterial ->
			if (craftingMaterial?.quantity != null && !craftingMaterial.item?.name.isNullOrEmpty()) {
				val skillView: View? =
					inflater.inflate(
						R.layout.item_crafting_material,
						this,
						false
					)

				val materialName: TextView? =
					skillView?.findViewById(R.id.armorDetailsCraftingMaterialName)

				val quantity: TextView? =
					skillView?.findViewById(R.id.armorDetailsCraftingMaterialQuantity)

				val materialDescription: TextView? =
					skillView?.findViewById(R.id.armorDetailsCraftingMaterialDescription)

				materialName?.text = craftingMaterial.item?.name
				materialDescription?.text = craftingMaterial.item?.description
				quantity?.text = craftingMaterial.quantity.toString().plus("X")

				this?.addView(skillView)
			}
		}
	}
}

@DrawableRes
fun returnArmorAssetByType(armorType: String?): Int? {
	return when (armorType?.lowercase()) {
		"head" -> R.drawable.ic_head
		"chest" -> R.drawable.ic_chest
		"gloves" -> R.drawable.ic_gloves
		"waist" -> R.drawable.ic_waist
		"legs" -> R.drawable.ic_legs
		else -> null
	}
}

@DrawableRes
fun returnArmorGender(attributes: ArmorAttributes?): Int {
	return when (attributes?.requiredGender?.lowercase()) {
		"female" -> R.drawable.ic_female
		"male" -> R.drawable.ic_male
		else -> R.drawable.ic_male_female
	}
}
