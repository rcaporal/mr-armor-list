package com.mr.armor.list.feature.view.adapters

import com.mr.armor.list.feature.data.models.Armor

interface ArmorCardClickListener {
	fun onArmorCardCLick(armor: Armor)
}
