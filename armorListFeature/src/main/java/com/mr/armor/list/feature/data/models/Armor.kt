package com.mr.armor.list.feature.data.models

data class Armor(
	val armorSet: ArmorSet? = null,
	val assets: ArmorAssets? = null,
	val attributes: ArmorAttributes? = null,
	val crafting: ArmorCrafting? = null,
	val defense: ArmorDefense? = null,
	val id: Int? = null,
	val name: String? = null,
	val rank: String? = null,
	val rarity: Int? = null,
	val resistances: ArmorResistances? = null,
	val skills: List<ArmorSkill?>? = null,
	val slots: List<ArmorSlot?>? = null,
	val type: String? = null
)
