package com.mr.armor.list.feature.data.models

data class ArmorDefense(
    val augmented: Int? = null,
    val base: Int? = null,
    val max: Int? = null
)
