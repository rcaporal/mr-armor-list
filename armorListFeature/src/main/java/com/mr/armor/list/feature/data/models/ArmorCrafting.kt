package com.mr.armor.list.feature.data.models

data class ArmorCrafting(
    val materials: List<CraftingMaterial?>?
)
