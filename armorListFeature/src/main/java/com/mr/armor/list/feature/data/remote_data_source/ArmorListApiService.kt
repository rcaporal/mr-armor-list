package com.mr.armor.list.feature.data.remote_data_source

import com.mr.armor.list.feature.data.models.Armor
import retrofit2.http.GET

interface ArmorListApiService {
	@GET("armor")
	suspend fun getArmorList(): List<Armor>
}
