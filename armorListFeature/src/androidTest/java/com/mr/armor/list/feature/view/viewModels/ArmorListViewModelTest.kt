package com.mr.armor.list.feature.view.viewModels

import com.mr.armor.list.feature.base.FakeArmorListRemoteDataSource
import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.data.remote_data_source.RequestState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Test
import java.io.IOException
import kotlin.test.assertIs

@OptIn(ExperimentalCoroutinesApi::class)
class ArmorListViewModelTest {

	private val fakeArmorName = "Plate Armor"

	@Test
	fun shouldGetAFakeArmorWithSuccess() = runTest {
		val armorListRemoteDataSource = FakeArmorListRemoteDataSource()
		val armorListViewModel = ArmorListViewModel(armorListRemoteDataSource)

		// Create an empty collector for the StateFlow
		val collectJob = launch(UnconfinedTestDispatcher()) {
			armorListViewModel.armorListResult.drop(1).collect{}
		}

		//Emits the request success
		armorListRemoteDataSource.emit(getFakeArmorWithSuccess())

		//Gets the result and verifies the success
		val result = armorListViewModel.armorListResult.value
		assertIs<RequestState.Success<List<Armor>>>(result)
		assertEquals(fakeArmorName, result.data.first().name)

		// Cancel the collecting job at the end of the test
		collectJob.cancel()
	}

	@Test
	fun shouldGetAnErrorAndThenRetryWithSuccess() = runTest {
		val armorListRemoteDataSource = FakeArmorListRemoteDataSource()
		val armorListViewModel = ArmorListViewModel(armorListRemoteDataSource)

		// Create an empty collector for the StateFlow
		val collectJob = launch(UnconfinedTestDispatcher()) {
			armorListViewModel.armorListResult.drop(1).collect {}
		}

		//Emits the request error
		armorListRemoteDataSource.emit(getFakeArmorWithError())

		//Gets the result and verifies the error
		assertIs<RequestState.Error>(armorListViewModel.armorListResult.value)

		//Emits the request success
		armorListRemoteDataSource.emit(getFakeArmorWithSuccess())

		//Reset the viewModel collector job
		armorListViewModel.tryAgain()

		//Gets the result and verifies the success
		val result = armorListViewModel.armorListResult.value
		assertIs<RequestState.Success<List<Armor>>>(result)
		assertEquals(fakeArmorName, result.data.first().name)

		// Cancel the collecting job at the end of the test
		collectJob.cancel()
	}

	private fun getFakeArmorWithError() = RequestState.Error(IOException())

	private fun getFakeArmorWithSuccess() =
		RequestState.Success(listOf(Armor(name = fakeArmorName)))

}
