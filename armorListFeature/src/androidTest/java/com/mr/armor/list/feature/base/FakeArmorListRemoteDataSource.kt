package com.mr.armor.list.feature.base

import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.data.remote_data_source.ArmorListRemoteDataSource
import com.mr.armor.list.feature.data.remote_data_source.RequestState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class FakeArmorListRemoteDataSource : ArmorListRemoteDataSource {
	private val flow = MutableSharedFlow<RequestState<List<Armor>>>()
	suspend fun emit(value: RequestState<List<Armor>>) = flow.emit(value)
	override fun getArmorList(): Flow<RequestState<List<Armor>>> = flow
}
