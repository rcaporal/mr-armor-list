package com.mr.armor.list.feature.utils

import org.junit.Assert.*

import org.junit.Test

class StringExtensionsTest {

	private val inputString: String = "Vespoid Greaves "
	private val inputCharSequence: CharSequence = "Vespoid Greaves "
	private val inputResult = "vespoidgreaves"

	private val nullInputString: String? = null
	private val nullInputCharSequence: CharSequence? = null
	private val nullInputResult: String = ""

    @Test
    fun `Should get a case-insensitive String without spaces from the input String`() {
		//GIVEN
		val input = inputString
		//WHEN
		val result = input.toSearchableContent()
		//THEN
		assertEquals(inputResult, result)
    }

    @Test
    fun `Should get a case-insensitive String without spaces from the input CharSequence`() {
		//GIVEN
		val input = inputCharSequence
		//WHEN
		val result = input.toSearchableContent()
		//THEN
		assertEquals(inputResult, result)
    }
	@Test
	fun `Should get an empty String from the null String input`() {
		//GIVEN
		val input = nullInputString
		//WHEN
		val result = input.toSearchableContent()
		//THEN
		assertEquals(nullInputResult, result)
	}

	@Test
	fun `Should get an empty String from the null CharSequence input`() {
		//GIVEN
		val input = nullInputCharSequence
		//WHEN
		val result = input.toSearchableContent()
		//THEN
		assertEquals(nullInputResult, result)
	}

}
