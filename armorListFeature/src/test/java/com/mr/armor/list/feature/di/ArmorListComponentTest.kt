package com.mr.armor.list.feature.di

import com.mr.armor.list.feature.data.remote_data_source.ArmorListApiService
import com.mr.armor.list.feature.data.remote_data_source.ArmorListRemoteDataSource
import com.mr.armor.list.feature.data.remote_data_source.ArmorListRemoteDataSourceImpl
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


fun dataModule(baseUrl: String) = module {
	factory {
		Retrofit.Builder()
			.baseUrl(baseUrl)
			.addConverterFactory(GsonConverterFactory.create())
			.build()
			.create(ArmorListApiService::class.java)
	}

	single<ArmorListRemoteDataSource> {
		ArmorListRemoteDataSourceImpl(
			armorListApiService =  get()
		)
	}
}
