package com.mr.armor.list.feature.data.remote_data_source

import com.google.gson.JsonSyntaxException
import com.mr.armor.list.feature.base.BaseWebserverUT
import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.di.dataModule
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection
import kotlin.test.assertIs

class ArmorListRemoteDataSourceTest : BaseWebserverUT() {

	override fun isMockServerEnabled(): Boolean = true

	override fun setUp() {
		super.setUp()
		startKoin {
			modules(dataModule(getMockUrl()))
		}
	}

	private val armorListRemoteDataSource by inject<ArmorListRemoteDataSource>()

	@Test
	fun `Should get armor list with 10 items and succeed`() = runBlocking {
		//STUB
		mockHttpResponse("get_armor_list_10.json", HttpURLConnection.HTTP_OK)

		armorListRemoteDataSource.getArmorList().collect {
			//GIVEN
			//WHEN
			val result: List<Armor> = (it as RequestState.Success).data
			//THEN
			result.forEach { armor -> assertNotNull(armor) }
			assertEquals(10, result.size)
		}
	}

	@Test
	fun `Should get JsonSyntaxException if API changes the contract`() = runBlocking {
		//STUB
		mockHttpResponse("get_armor_list_wrong_contract.json", HttpURLConnection.HTTP_OK)

		armorListRemoteDataSource.getArmorList().collect {
			//GIVEN
			//WHEN
			val exception: Exception? = (it as RequestState.Error).exception
			//THEN
			assertIs<JsonSyntaxException>(exception)
		}
	}

	@Test
	fun `Should get IOException if there is a request timeout`() = runBlocking {
		//STUB
		mockHttpResponse("get_armor_list_10.json", HttpURLConnection.HTTP_CLIENT_TIMEOUT)

		armorListRemoteDataSource.getArmorList().collect {
			//GIVEN
			//WHEN
			val exception: Exception? = (it as RequestState.Error).exception
			//THEN
			assertIs<IOException>(exception)
		}
	}

	@Test
	fun `Should get HttpException if there is a server-side problem`() = runBlocking {
		//STUB
		mockHttpResponse("get_armor_list_10.json", HttpURLConnection.HTTP_INTERNAL_ERROR)

		armorListRemoteDataSource.getArmorList().collect {
			//GIVEN
			//WHEN
			val exception: Exception? = (it as RequestState.Error).exception
			//THEN
			assertIs<HttpException>(exception)
		}
	}


}
