package com.mr.armor.list.feature.utils

import com.mr.armor.list.feature.data.models.Armor
import com.mr.armor.list.feature.data.models.ArmorDefense
import com.mr.armor.list.feature.data.models.ArmorSet
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ArmorSearchExtensionsTest {

	private val armorList: List<Armor> = listOf(
		Armor(
			name = "Leather Headgear 2",
			type = "head",
			defense = ArmorDefense(base = 1),
			rank = "low",
			armorSet = ArmorSet(name = "Leather")
		),
		Armor(
			name = "Chainmail Vest",
			type = "chest",
			defense = ArmorDefense(base = 2),
			rank = "low",
			armorSet = ArmorSet(name = "Chainmail")
		),
		Armor(
			name = "Leather Belt Legs",
			type = "chest",
			defense = ArmorDefense(base = 3),
			rank = "medium",
			armorSet = ArmorSet(name = "Leather")
		),
		Armor(
			name = "Chainmail Trousers",
			type = "legs",
			defense = ArmorDefense(base = 4),
			rank = "high",
			armorSet = ArmorSet(name = "Ghost")
		),
	)


	@Test
	fun `Should search for 'chainmailvest' and get at least the Chainmail Vest armor`() {
		//GIVEN
		val query: CharSequence = "chainmailvest"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertNotNull(result.find { it.name?.contains("Chainmail Vest") == true })
	}

	@Test
	fun `Should search for 'Chainmail Vest' and get at least the Chainmail Vest armor`() {
		//GIVEN
		val query: CharSequence = "Chainmail Vest"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertNotNull(result.find { it.name?.contains("Chainmail Vest") == true })
	}

	@Test
	fun `Should search for 'Leatherheadgear2' and get at least the Leather Headgear 2 armor`() {
		//GIVEN
		val query: CharSequence = "Leatherheadgear2"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertNotNull(result.find { it.name?.contains("Leather Headgear 2") == true })
	}

	@Test
	fun `Should search for 'ghost' and get at least the armor of the Ghost set`() {
		//GIVEN
		val query: CharSequence = "ghost"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertNotNull(result.find { it.armorSet?.name?.contains("Ghost") == true })
	}

	@Test
	fun `Should search for 'high' and get at least the armor with rank type equal to high`() {
		//GIVEN
		val query: CharSequence = "high"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertNotNull(result.find { it.rank?.contains("high") == true })
	}

	@Test
	fun `Should search for 'head' and get at least the armor with type equal to head`() {
		//GIVEN
		val query: CharSequence = "head"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertNotNull(result.find { it.type?.contains("head") == true })
	}

	@Test
	fun `Should search for '2' and get all the armors with base defense greater or equal to 2`() {
		//GIVEN
		val query: CharSequence = "2"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		result.forEach {
			assert((it.defense?.base ?: 0) >= 2)
		}
	}

	@Test
	fun `Should search for '2000' and don't get any armor`() {
		//GIVEN
		val query: CharSequence = "2000"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		result.forEach {
			assert((it.defense?.base ?: 0) >= 2000)
		}
	}

	@Test
	fun `Should search for 'abcdef' and don't get any armor`() {
		//GIVEN
		val query: CharSequence = "abcdef"
		//WHEN
		val result = armorList.filterArmorsByQuery(query)
		//THEN
		assertEquals(0, result.size)
	}


}
