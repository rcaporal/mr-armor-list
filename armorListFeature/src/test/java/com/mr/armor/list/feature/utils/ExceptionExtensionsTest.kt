package com.mr.armor.list.feature.utils

import android.content.Context
import androidx.annotation.StringRes
import com.google.gson.JsonSyntaxException
import com.mr.armor.list.feature.R
import okio.IOException
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import java.net.UnknownHostException


@RunWith(MockitoJUnitRunner::class)
class ExceptionExtensionsTest {

	@Mock
	private lateinit var mockContext: Context

	private var fakeNetworkProblemMessage: String = "fakeNetworkProblemMessage"
	private var fakeJsonSyntaxExceptionMessage: String = "fakeJsonSyntaxExceptionMessage"
	private var fakeGeneralExceptionMessage: String = "fakeGeneralExceptionMessage"

	private fun mockGetString(@StringRes stringRes: Int, fakeString: String) {
		`when`(mockContext.getString(stringRes))
			.thenReturn(fakeString)
	}

    @Test
    fun `Should get a correct network-related message from the IOException`() {
		//STUB
		mockGetString(R.string.request_no_internet_error_message, fakeNetworkProblemMessage)
		//GIVEN
		val iOException = IOException()
		//WHEN
		val message = mockContext.getString(iOException.networkErrorMessageResource())
		//THEN
		assertEquals(fakeNetworkProblemMessage, message)
    }

	@Test
    fun `Should get a correct network-related message from the UnknownHostException`() {
		//STUB
		mockGetString(R.string.request_no_internet_error_message, fakeNetworkProblemMessage)
		//GIVEN
		val unknownHostException = UnknownHostException()
		//WHEN
		val message = mockContext.getString(unknownHostException.networkErrorMessageResource())
		//THEN
		assertEquals(fakeNetworkProblemMessage, message)
    }

	@Test
    fun `Should get a correct network-related message from the JsonSyntaxException`() {
		//STUB
		mockGetString(R.string.request_serve_side_error_message, fakeJsonSyntaxExceptionMessage)
		//GIVEN
		val jsonSyntaxException = JsonSyntaxException("")
		//WHEN
		val message = mockContext.getString(jsonSyntaxException.networkErrorMessageResource())
		//THEN
		assertEquals(fakeJsonSyntaxExceptionMessage, message)
    }

	@Test
    fun `Should get a correct network-related message from general Exceptions`() {
		//STUB
		mockGetString(R.string.request_error_message, fakeGeneralExceptionMessage)
		//GIVEN
		val nullPointerException = NullPointerException()
		//WHEN
		val message = mockContext.getString(nullPointerException.networkErrorMessageResource())
		//THEN
		assertEquals(fakeGeneralExceptionMessage, message)
    }
}
