package com.mr.armor.list.feature.base

import okhttp3.mockwebserver.MockResponse
import java.io.File

abstract class BaseWebserverUT : BaseKoinWebserverTest() {

	fun mockHttpResponse(fileName: String, responseCode: Int) = mockServer.enqueue(
		MockResponse()
			.setResponseCode(responseCode)
			.setBody(getJson(fileName))
	)

	private fun getJson(path: String): String {
		val uri = javaClass.classLoader?.getResource(path)
		val file = uri?.path?.let { File(it) }
		return if (file != null) {
			String(file.readBytes())
		} else String()
	}
}
