# Mr Armor List

This application consumes data from the [Monster Hunter game's Armor API](https://mhw-db.com/) and displays it in a list format.
It's possible to see the details of each armor, such as rarity, resistance against elements, and items for crafting.
Another feature is to search for armor by name, base defense, ranking, type, and set.

![screens](https://i.imgur.com/fMoEgaL.png)

## Technical Aspects

This application was structured to support changes quickly and have high code reuse and resource optimization.

To be reused easily, it was divided into two modules. The ```app``` module consists of the host application whit MainActivity, and the ```armorListFeature``` module comprises the search/display fragments and armor details fragments. You can reuse the created Fragments by importing the   ```armorListFeature``` module in any other application.

Thinking about possible changes, the module ```armorListFeature``` was developed using the MVVM architecture with classes divided by responsibilities. The aspect that would help with the likely changes is that the application layers are separated by interfaces that have their implementations injected using Koin. Most of the libraries used in the app have been placed inside kotlin extensions, making changing the library easy. For example, the image-loading library can easily change as it is encapsulated inside an extension function. Unit and instrumentation tests were also developed to help with future code refactoring.

The ArmorListFragment was developed using ViewModel and StateFlows. When rotating the screen, the Stateflow stores the data and subsequently delivers them to the new screen created, without requiring a new data request to the API. This causes resources to be optimized since the data request is made only once.

![StateFlowImage](https://i.imgur.com/A0kkuIz.png)

All this is done by considering the Fragment's lifecycle, avoiding possible crashes. Another point to be highlighted is that the real-time search has an interval so that any letter typed by the user does not trigger it.

![screens](https://media1.tenor.com/images/61c2b218d7b5c146c433b6f016ee8e56/tenor.gif?itemid=27127135)

## Android Libraries

These are the external libraries used in this application:

- Retrofit2
- Kotlin Flow
- Kotlin Coroutines
- ViewModel
- Picasso
- MaterialDialogs
- FlexBox
- Navigation
- Gson
- Koin
