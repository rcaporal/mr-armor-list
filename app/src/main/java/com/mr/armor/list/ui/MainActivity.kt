package com.mr.armor.list.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mr.armor.list.R
import kotlinx.android.synthetic.main.layout_main_toolbar.*

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		if (supportActionBar == null) {
			setSupportActionBar(mainToolbar)
		}
	}
}
