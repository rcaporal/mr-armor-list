package com.mr.armor.list

import android.app.Application
import com.mr.armor.list.feature.di.armorListModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class ArmorListApplication : Application() {

	override fun onCreate() {
		super.onCreate()
		initDI()
	}

	private fun initDI() {
		val appModules = mutableListOf<Module>().apply {
			addAll(armorListModules)
		}

		startKoin {
			androidContext(this@ArmorListApplication)
			modules(appModules)
		}
	}

}
